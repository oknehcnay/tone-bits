package com.yanchenko.tb

import com.google.common.truth.Truth.assertThat
import com.yanchenko.tb.Spec.introOutro
import com.yanchenko.tb.Spec.oneLengthMicrosecond
import com.yanchenko.tb.Spec.startByte1
import com.yanchenko.tb.Spec.startByte2
import com.yanchenko.tb.Spec.zeroLengthMicrosecond
import com.yanchenko.tb.parser.boolsToByte
import com.yanchenko.tb.parser.byteFromStream
import com.yanchenko.tb.parser.isZeroCross
import com.yanchenko.tb.parser.sampleCount
import com.yanchenko.tb.util.binaryString
import org.junit.Test

class BitParserTest {

    @Test
    fun `boolsToByte() decodes correctly`() {
        arrayOf("01111111", "01010100").forEach { byteStr ->
            val byte = boolsToByte(byteStr.toBoolArr())
            assertThat(byte.binaryString())
                .isEqualTo(byteStr)
        }
    }

    @Test
    fun `boolsToByte() decodes in big-endian encoding`() {
        arrayOf(
            "01000010" to startByte1,
            "00000011" to startByte2,
            "11111111" to introOutro
        ).forEach { (byteStr, byte) ->
            assertThat(boolsToByte(byteStr.toBoolArr()))
                .isEqualTo(byte)
        }
    }

    @Test
    fun `Sample count counts`() {
        val sampleRate = 44_100
        mapOf(
            oneLengthMicrosecond to 14,
            zeroLengthMicrosecond to 28
        ).forEach { (spec, ticks) ->
            assertThat(sampleCount(sampleRate, spec)).isEqualTo(ticks)
        }
    }

    @Test
    fun `Zero cross check checks`() {

        val noCross = mapOf(0 to 0, 128 to 128)
        val cross = mapOf(-1 to 0, 1 to 0, 1 to -1)

        noCross.forEach { (from, to) ->
            assertThat(isZeroCross(from.toShort(), to.toShort()))
                .isFalse()
        }

        cross.forEach { (from, to) ->
            assertThat(isZeroCross(from.toShort(), to.toShort()))
                .isTrue()
        }
    }

    @Test
    fun `byteFromStream() returns null if doesn't start with 0`() {
        val list = "11111111111".toBoolArr().toList()
        assertThat(byteFromStream(list))
            .isNull()
    }

    @Test
    fun `byteFromStream() returns null if too short`() {
        val list = "01".toBoolArr().toList()
        assertThat(byteFromStream(list))
            .isNull()
    }

    @Test
    fun `byteFromStream() works for valid input`() {
        mapOf(
            "00100001011" to startByte1,
            "01100000011" to startByte2,
            "01111111111" to introOutro
        ).forEach { (str, byte) ->
            val b = byteFromStream(str.toBoolArr().toList())
            assertThat(b)
                .isEqualTo(byte)
        }
    }
}
