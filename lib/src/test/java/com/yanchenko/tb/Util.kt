package com.yanchenko.tb

fun String.toBoolArr() = toCharArray().map {
    when (it) {
        '0' -> false
        '1' -> true
        else -> TODO("$it")
    }
}.toBooleanArray()