package com.yanchenko.tb.util

fun Byte.binaryString() =
    Integer.toBinaryString(toInt() and 0xFF)
        .let { String.format("%8s", it) }
        .replace(' ', '0')

fun Byte.hexString() = String.format("%02X", this)
