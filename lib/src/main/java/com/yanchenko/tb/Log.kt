package com.yanchenko.tb

object Log {
    var delegate: ((String) -> Unit)? = null
    fun d(msg: Any?) {
        delegate?.invoke(msg.toString()) ?: System.out.println(msg)
    }
}