package com.yanchenko.tb

import java.nio.ByteOrder

object Spec {
    val byteOrder: ByteOrder = ByteOrder.LITTLE_ENDIAN

    val oneLengthMicrosecond = 320
    val zeroLengthMicrosecond = 640

    val bitsInBytePacket = 11

    //

    val introOutro = 0xFF.toByte()
    val startByte1 = 0x42.toByte()
    val startByte2 = 0x03.toByte()

    val messageCount = 64
    val dataBytes = 30
    val checksumByte = 1
    val endByte = 0x00.toByte()
}