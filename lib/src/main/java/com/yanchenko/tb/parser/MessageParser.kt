package com.yanchenko.tb.parser

import com.yanchenko.tb.Spec.checksumByte
import com.yanchenko.tb.Spec.dataBytes
import com.yanchenko.tb.Spec.endByte
import com.yanchenko.tb.Spec.introOutro
import com.yanchenko.tb.Spec.messageCount
import com.yanchenko.tb.Spec.startByte1
import com.yanchenko.tb.Spec.startByte2
import com.yanchenko.tb.parser.State.*

/**
 * Builds messages from bytes.
 */
class MessageParser(private val stateCb: (State) -> Unit, cb: (Message) -> Unit) :
    AbstractParser<Byte, Message>(cb) {

    private val messageBytes = mutableListOf<Byte>()

    private var msgCount = 0

    private var state = PreIntro
        private set(value) {
            field = value
            stateCb(value)
        }

    override fun handle(byte: Byte) {
        if (state.isBefore(Data)) {
            state.nextState().takeIf { it.matches(byte) }?.let { nextState ->
//                Log.d("Got byte $byte, moving to state $nextState")
                state = nextState
            }
        }
        if (state != Data) return
        messageBytes.add(byte)
        if (messageBytes.size == (dataBytes + checksumByte)) {
            msgCount++
            val msg =
                Message(
                    msgCount,
                    messageBytes.subList(0, messageBytes.size - 1).toByteArray(),
                    messageBytes.last()
                )
            cb(msg)
//            Log.d("MSG ${msgCount} ${msg.isValid}")
            messageBytes.clear()
        }
        if (msgCount == messageCount && Outro.matches(byte)) {
//            Log.d("Got $msgCount messages, data complete.")
            state = Outro
        }
    }
}

enum class State(val matches: (Byte) -> Boolean) {
    PreIntro({ false }),
    Intro({ it == introOutro }),
    StartByteOne({ it == startByte1 }), StartByteTwo({ it == startByte2 }),
    Data({ it != startByte2 }),
    Outro({ it == endByte });

    fun isBefore(other: State) = ordinal < other.ordinal
    fun nextState() = values()[ordinal + 1]
}

data class Message(val num: Int, val data: ByteArray, val checksum: Byte) {

    val isValid = (data.sum().toByte() == checksum)

}
