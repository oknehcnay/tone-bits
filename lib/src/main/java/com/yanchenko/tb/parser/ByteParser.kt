package com.yanchenko.tb.parser

import com.yanchenko.tb.Spec.bitsInBytePacket

/**
 * Builds bytes from bits.
 */
class ByteParser(cb: (Byte) -> Unit) : AbstractParser<Boolean, Byte>(cb) {

    private val bits = mutableListOf<Boolean>()

    override fun handle(bit: Boolean) {
        bits.add(bit)
        byteFromStream(bits)?.let { newByte ->
            cb(newByte)
            bits.clear()
        }
    }
}

/**
 *|0|b0|b1|b2|b3|b4|b5|b6|b7|1|1|
 */
fun byteFromStream(bits: List<Boolean>): Byte? {
    val len = bits.size
    if (len >= bitsInBytePacket) {
        // last 2 bits true(1)
        if (bits[len - 1] && bits[len - 2]) {
            // first bit false(0)
            if (!bits[len - bitsInBytePacket]) {
                return bits.subList(len - bitsInBytePacket + 1, len - 2).toBooleanArray()
                    .apply { reverse() /*little-endian*/ }
                    .let(::boolsToByte)
            }
        }
    }
    return null
}

fun boolsToByte(arr: BooleanArray): Byte {
    assert(arr.size == 8) { "Should be 8, got ${arr.size}" }
    return arr.foldIndexed(0) { idx, byte, bitSet ->
        if (bitSet) {
            byte or (128 shr idx) // byte |= (128 >> idx)
        } else byte
    }.toByte()
}
