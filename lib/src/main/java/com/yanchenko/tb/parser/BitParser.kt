package com.yanchenko.tb.parser

import com.yanchenko.tb.Log
import com.yanchenko.tb.Spec.oneLengthMicrosecond
import com.yanchenko.tb.Spec.zeroLengthMicrosecond
import java.nio.ShortBuffer
import kotlin.math.abs
import kotlin.math.sign

/**
 * Extracts individual bits from audio stream.
 */
class BitParser(
    sampleRateHz: Int,
    private val channelCount: Int,
    cb: (Boolean) -> Unit
) : AbstractParser<ShortBuffer, Boolean>(cb) {

    private val zeroSamples = sampleCount(sampleRateHz, zeroLengthMicrosecond)
    private val oneSamples = sampleCount(sampleRateHz, oneLengthMicrosecond)

    private var prev: Short = -1
    private var count: Int = 0

    override fun handle(samples: ShortBuffer) {
        for (i in 0 until samples.limit() step channelCount) {
            val curr = samples.get(i)
            Log.d("Idx: $i, val $curr.")
            val crossed = isZeroCross(prev, curr)
            if (crossed) {
                val zeroOrOne = isZeroOrOne(count)
                Log.d("Count: $count, $zeroOrOne")
                zeroOrOne?.let(cb)
                count = 0
            }
            count++
            prev = curr
        }
    }

    private fun isZeroOrOne(num: Int) = when {
        isCloseTo(zeroSamples, num) -> false
        isCloseTo(oneSamples, num) -> true
        else -> null
    }
}

fun isZeroCross(prev: Short, curr: Short) =
    //could use bitshift if profiling shows it's a bottleneck
    prev.toInt().sign != curr.toInt().sign

fun isCloseTo(target: Int, num: Int, threshold: Int = 2) =
    //could use bitshift if profiling shows it's a bottleneck
    abs(target - num) < threshold

fun sampleCount(sampleRateHz: Int, lengthMicrosecond: Int) =
    (sampleRateHz / 1_000_000.0 * lengthMicrosecond).toInt()