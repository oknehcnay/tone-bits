package com.yanchenko.tb.parser

/**
 * A parser takes input through it's handle function and calls back when a result is available.
 */
abstract class AbstractParser<IN, OUT>(protected val cb: (OUT) -> Unit) {
    abstract fun handle(item: IN)
}