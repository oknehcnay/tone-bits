# ToneBits
Reads FSK-encoded data from bundled .wav files and displays to the user.

![Screen](screen.png)

Consists of Kotlin:
- *lib* with parsing logic responsible for converting *PCM* data into *Messages*
- *app* that uses *Exo Player* to playback the files and feed bytes to the lib.

Developed using Android Studio 4.2 & a Pixel 3a running Android 11.

## Lib
The parsing itself is split into 3 layers: *bits*, *bytes* and *messages*.
Each parser is fed appropriate input data and calls back when the next element is ready.

## App
A single-Activity Android app (would evolve to use Navigation + Fragments for further screens).
Uses MVVM with *ViewModel*s and *LiveData*.
Also *ViewBinding*s as synthetic views are deprecated since Kotlin 1.4.20.

## TODO
Add Bit, Message parser tests.

