package com.yanchenko.tb.adapter

import android.media.AudioFormat
import com.google.android.exoplayer2.audio.AudioProcessor
import com.google.android.exoplayer2.audio.TeeAudioProcessor
import com.yanchenko.tb.Log
import com.yanchenko.tb.Spec
import com.yanchenko.tb.parser.*
import java.nio.ByteBuffer

/**
 * ExoPlayer sink that parses messages from audio.
 * @param stateCb - callback for parsing state changes.
 * @param messageCb - callback for messages.
 */
class ParsingSink(private val stateCb: (State) -> Unit, private val messageCb: (Message) -> Unit) :
    TeeAudioProcessor.AudioBufferSink {

    private lateinit var bitParser: BitParser
    private lateinit var byteParser: ByteParser
    private lateinit var messageParser: MessageParser

    override fun flush(sampleRateHz: Int, channelCount: Int, encoding: Int) {
        Log.i("sampleRateHz: $sampleRateHz, channelCount: $channelCount, encoding: $encoding")
        if (encoding != AudioFormat.ENCODING_PCM_16BIT) {
            throw AudioProcessor.AudioFormat(sampleRateHz, channelCount, encoding)
                .let(AudioProcessor::UnhandledAudioFormatException)
        }
        messageParser = MessageParser(stateCb, messageCb)
        byteParser = ByteParser(messageParser::handle)
        bitParser = BitParser(sampleRateHz, channelCount, byteParser::handle)
    }

    override fun handleBuffer(buffer: ByteBuffer) {
//            Log.d("buffer: $buffer, order: ${buffer.order()}")
        val b = buffer
            .order(Spec.byteOrder)
            .asShortBuffer()
        bitParser.handle(b)
    }
}