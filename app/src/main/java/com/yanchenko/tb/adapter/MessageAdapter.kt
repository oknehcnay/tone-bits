package com.yanchenko.tb.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.yanchenko.tb.coloredSpan
import com.yanchenko.tb.databinding.ViewListRowBinding
import com.yanchenko.tb.parser.Message
import com.yanchenko.tb.util.hexString

/**
 * Displays Messages in the RecyclerView as cards with pretty backgrounds.
 * Each contains message number, data bytes and checksum,
 * the background of which is green if it matches or red if it doesn't.
 */
class MessageAdapter : ListAdapter<Message, MessageAdapter.VH>(diffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewListRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
            .let(MessageAdapter::VH)

    override fun onBindViewHolder(holder: VH, position: Int) {
        val message = getItem(position)
        with(holder.binding) {
            numberTv.text = "${message.num}."
            dataTv.text = message.data.map(Byte::hexString).toTypedArray()
                .joinToString(" ")
            checksumTv.text = coloredSpan(
                message.checksum.hexString(),
                bgColor = if (message.isValid) Color.GREEN else Color.RED
            )
            colorCard(cardView, position)
        }
    }

    companion object {
        val diffCallback = object : DiffUtil.ItemCallback<Message>() {
            override fun areItemsTheSame(oldItem: Message, newItem: Message) =
                (oldItem.num == newItem.num)

            override fun areContentsTheSame(oldItem: Message, newItem: Message) =
                oldItem.data.contentEquals(newItem.data)
        }

        private fun colorCard(cv: CardView, pos: Int) {
            val cardBg =
                if (pos % 2 == 0) android.R.color.holo_blue_light else android.R.color.holo_green_light
            cv.setCardBackgroundColor(ContextCompat.getColor(cv.context, cardBg))
        }
    }

    class VH(val binding: ViewListRowBinding) : RecyclerView.ViewHolder(binding.root)
}