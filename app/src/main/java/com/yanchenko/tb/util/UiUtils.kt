package com.yanchenko.tb

import android.net.Uri
import android.text.Spannable
import android.text.SpannableString
import android.text.style.BackgroundColorSpan
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.annotation.ColorInt

/**
 * Add RadioButtons to the group for all files in the specified assets dir.
 * Calls back with Uri of the selected file or null.
 */
fun addFileRadiobuttons(rg: RadioGroup, onSelected: (Uri?) -> Unit, audioAssetDir: String = "wav") {
    val ctx = rg.context
    val fileList =
        ctx.assets.list(audioAssetDir)
            ?: TODO("handle null file list")
    fileList.onEachIndexed { idx, fileName ->
        RadioButton(ctx).apply {
            id = idx
            text = fileName
        }.let(rg::addView)
    }
    rg.setOnCheckedChangeListener { _, checkedId ->
        val uri =
            fileList.getOrNull(checkedId)
                ?.let { fileName -> Uri.parse("assets:///$audioAssetDir/$fileName") }
        onSelected(uri)
    }
}

/**
 * Creates a SpannableString with colored background.
 */
fun coloredSpan(txt: String, @ColorInt bgColor: Int) =
    SpannableString(txt).apply {
        setSpan(
            BackgroundColorSpan(bgColor),
            0, txt.length,
            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
        )
    }
