package com.yanchenko.tb.util

import java.lang.ref.WeakReference
import kotlin.reflect.KProperty

/**
 * Delegate for making usage of [WeakReference] from Kotlin more convenient.
 */
class WeakRef<T>(initial: T? = null) {
    private var ref: WeakReference<T>? = null

    init {
        initial?.run {
            ref = WeakReference(this)
        }
    }

    operator fun getValue(thisRef: Any?, property: KProperty<*>) = ref?.get()
    operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T?) {
        ref = value?.let { WeakReference(it) }
    }
}
