package com.yanchenko.tb

import timber.log.Timber

typealias Log = Timber

fun wtf(any: Any?) = Log.wtf(any.toString())
