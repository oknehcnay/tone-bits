package com.yanchenko.tb

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.yanchenko.tb.adapter.MessageAdapter
import com.yanchenko.tb.databinding.ActivityMainBinding
import com.yanchenko.tb.vm.injectedViewModels

class MainActivity : AppCompatActivity() {

    private val playbackViewModel by injectedViewModels<MessagePlaybackViewModel>()
    private val messageAdapter by lazy { MessageAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ActivityMainBinding.inflate(layoutInflater).let { binding ->
            setContentView(binding.root)
            bindUi(binding)
        }
    }

    private fun bindUi(binding: ActivityMainBinding) {
        addFileRadiobuttons(binding.fileSelectionRg, playbackViewModel::play)
        binding.speedSlider.addOnChangeListener { _, value, _ ->
            binding.fileSelectionRg.clearCheck()
            playbackViewModel.setSpeed(value)
        }
        binding.speedSlider.value = 1f
        binding.messagesRV.adapter = messageAdapter

        playbackViewModel.state.observe(this) { state ->
            binding.statusTv.text = state?.toString() ?: ""
        }
        playbackViewModel.messages.observe(this) { nextMsg ->
            val messageList = nextMsg?.let {
                messageAdapter.currentList + nextMsg
            } ?: emptyList()
            messageAdapter.submitList(messageList)
            binding.messagesRV.smoothScrollToPosition(messageAdapter.itemCount)
        }
    }
}

