package com.yanchenko.tb.vm

import androidx.annotation.MainThread
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.AbstractSavedStateViewModelFactory
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelLazy
import com.yanchenko.tb.MessagePlaybackViewModel
import com.yanchenko.tb.adapter.ParsingSink
import com.yanchenko.tb.parser.Message
import com.yanchenko.tb.parser.State
import com.yanchenko.tb.util.WeakRef

@MainThread
inline fun <reified VM : ViewModel> Fragment.activityInjectedViewModels() =
    ViewModelLazy(
        VM::class,
        { requireActivity().viewModelStore }) { VmFactory(requireActivity() as AppCompatActivity) }

@MainThread
inline fun <reified VM : ViewModel> AppCompatActivity.injectedViewModels() =
    ViewModelLazy(VM::class, { viewModelStore }) { VmFactory(this) }

class VmFactory(act: AppCompatActivity) : AbstractSavedStateViewModelFactory(act, null) {

    private val app by WeakRef(act.application)

    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel> create(
        key: String, modelClass: Class<T>, handle: SavedStateHandle
    ) = app?.let {
        when (modelClass.kotlin) {
            MessagePlaybackViewModel::class -> MessagePlaybackViewModel(it, ::sinkProvider)
            else -> TODO("$modelClass")
        } as T
    } ?: TODO("null app")
}

private fun sinkProvider(stateCb: (State) -> Unit, messageCb: (Message) -> Unit) =
    ParsingSink(stateCb, messageCb)
