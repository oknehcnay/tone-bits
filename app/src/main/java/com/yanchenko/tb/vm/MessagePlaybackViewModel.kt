package com.yanchenko.tb

import android.app.Application
import android.content.Context
import android.net.Uri
import androidx.annotation.FloatRange
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.google.android.exoplayer2.DefaultRenderersFactory
import com.google.android.exoplayer2.MediaItem
import com.google.android.exoplayer2.PlaybackParameters
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.audio.AudioCapabilities
import com.google.android.exoplayer2.audio.DefaultAudioSink
import com.google.android.exoplayer2.audio.TeeAudioProcessor
import com.google.android.exoplayer2.source.DefaultMediaSourceFactory
import com.google.android.exoplayer2.upstream.AssetDataSource
import com.yanchenko.tb.adapter.ParsingSink
import com.yanchenko.tb.parser.Message
import com.yanchenko.tb.parser.State

class MessagePlaybackViewModel(
    app: Application,
    sinkProvider: (stateCb: (State) -> Unit, messageCb: (Message) -> Unit) -> ParsingSink
) : AndroidViewModel(app) {

    /**
     * Message parsing state.
     */
    val state = MutableLiveData<State?>()

    /**
     * Next parsed message from the stream.
     */
    val messages = MutableLiveData<Message?>()

    private val player by lazy {
        val parsinSink = sinkProvider(state::postValue, messages::postValue)
        SimpleExoPlayer.Builder(app, SinkingRenderersFactory(app, parsinSink))
            .setMediaSourceFactory(DefaultMediaSourceFactory { AssetDataSource(app) })
            .build()
    }

    override fun onCleared() {
        super.onCleared()
        player.release()
    }

    /**
     * Play uri or stop if null.
     */
    fun play(filePath: Uri?) {
        Log.i("play $filePath")
        state.value = null
        messages.value = null
        filePath?.let {
            MediaItem.fromUri(filePath)
                .let(player::setMediaItem)
            player.play()
            player.prepare()
        } ?: player.stop()
    }

    /**
     * Playback speed.
     */
    fun setSpeed(@FloatRange(from = 0.0, to = 1.0) sp: Float) {
        Log.i("Set speed $sp")
        if (sp > 0)
            player.setPlaybackParameters(PlaybackParameters(sp))
    }
}

class SinkingRenderersFactory(
    ctx: Context,
    private val sink: TeeAudioProcessor.AudioBufferSink
) : DefaultRenderersFactory(ctx) {
    override fun buildAudioSink(
        context: Context,
        enableFloatOutput: Boolean,
        enableAudioTrackPlaybackParams: Boolean,
        enableOffload: Boolean
    ) = DefaultAudioSink(
        AudioCapabilities.getCapabilities(context),
        DefaultAudioSink.DefaultAudioProcessorChain(TeeAudioProcessor(sink)),
        enableFloatOutput,
        enableAudioTrackPlaybackParams,
        enableOffload
    )
}

